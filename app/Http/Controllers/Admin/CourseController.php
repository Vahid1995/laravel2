<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CourseRequest;

class CourseController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::latest()->paginate(20);
        return view('Admin.Courses.index',compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $request)
    {
        /*return User::create([
            'name' => 'vahid',
            'level' => 'admin',
            'email' => 'vahid@gmail.com',
            'password' => bcrypt(1234)
        ]);*/
        auth()->loginUsingId(1);

        $imagesUrl = $this->uploadImages($request->file('images'));
        auth()->user()->course()->create(array_merge($request->all(),['images' => $imagesUrl]));
        return redirect(route('courses.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $Course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $Course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        return view('Admin.Courses.edit',compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(CourseRequest $request, Course $course)
    {
        $file = $request->file('images');
        $inputs = $request->all();
        if ($file){
            $inputs['images']  = $this->uploadImages($request->file('images'));
        }else{
            $inputs['images'] = $course->images;
            $inputs['images']['thumb'] = $inputs['imagesThumb'];
        }

        unset($inputs['imagesThumb']);
        $course->update($inputs);


        return redirect(route('courses.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        $course->delete();
        return redirect(route('courses.index'));
    }
}
