<?php

/**********************************ADMIN*************************************/


use Carbon\Carbon;

Route::namespace('Admin')->prefix('admin')->group(function (){
   $this->get('/panel','PanelController@index');
   $this->post('/panel/upload_image' , 'panelController@uploadImageSubject')->name('upload');
   $this->resource('/articles','ArticleController');
   $this->resource('/courses','CourseController');
   $this->resource('/episodes','EpisodeController');

});


//Route::resource('article');
/**********************************ADMIN*************************************/
