@extends('Admin.master')

@section('content')

    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title">مقالات</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- BORDERED TABLE -->
                    <div class="panel">
                        <div class="panel-heading">
                            <a href="{{route('articles.create')}}"  class="btn btn-primary">ایجاد مقاله</a>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <thead>
                                <tr>

                                    <th>عنوان مقاله</th>
                                    <th>تعداد نظرات</th>
                                    <th>مقدار بازدید</th>
                                    <th>تنظیمات</th>

                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($articles as $article)
                                        <tr>
                                            <td><a href="{{$article->path()}}">{{$article->title}}</a> </td>
                                            <td>{{$article->commentCount}}</td>
                                            <td>{{$article->viewCount}}</td>
                                            <td>
                                                <form action="{{ route('articles.destroy' , ['id' => $article->id]) }}" method="post">
                                                    {{method_field('delete')}}
                                                    {{csrf_field()}}
                                                    <div class="btn-group btn-group-lg">
                                                        <a href="{{route('articles.edit',['id' => $article->id])}}"  class="btn-sm btn-primary">ویرایش</a>
                                                        <button type="submit" class="btn-sm btn-danger">حذف</button>
                                                    </div>

                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div style="text-align: center">
                            {{ $articles->render() }}
                        </div>
                    </div>
                    <!-- END BORDERED TABLE -->
                </div>
            </div>
        </div>
    </div>


@endsection