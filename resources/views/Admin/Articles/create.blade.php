@extends('Admin.master')

@section('content')

    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title">ایجاد مقاله</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- BORDERED TABLE -->
                    <div class="panel">

                        <div class="panel-body">
                            <form action="{{ route('articles.store') }}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                @include('Admin.error')
                                <div class="form-group col-md-12">
                                    <label for="title">عنوان مقاله</label>
                                    <input type="text" class="form-control" placeholder="عنوان مقاله" name="title" id="title" value="{{old('title')}}">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="description">توضیحات</label>
                                    <textarea class="form-control" rows="5" name="description" id="description">{{old('description')}}</textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="body">متن</label>
                                    <textarea rows="5" class="form-control" placeholder="متن" name="body" id="body">{{old('body')}}</textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-sm-6">
                                        <label for="tags">برچسب ها</label>
                                        <input type="text" class="form-control" placeholder="برچسب ها" name="tags" id="tags" value="{{old('tags')}}">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="images">تصاویر</label>
                                        <input type="file" class="form-control" placeholder="تصاویر" name="images" id="images">
                                    </div>

                                </div>
                                <div class="form-group col-md-12">
                                   <input type="submit" value="ثبت مقاله" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END BORDERED TABLE -->
                </div>
            </div>
        </div>
    </div>


@endsection

@section('footer')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        window.onload = function() {
            CKEDITOR.replace( 'body', {
                filebrowserUploadUrl: '{{ route('upload',['_token' => csrf_token() ]) }}'
            });
        };
    </script>
@endsection

