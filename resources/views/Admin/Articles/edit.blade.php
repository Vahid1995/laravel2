@extends('Admin.master')

@section('content')

    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title">ویرایش مقاله</h3>
            <a href="{{route('articles.index')}}" class="btn-sm btn-primary pull-left">بازگشت</a>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- BORDERED TABLE -->
                    <div class="panel">

                        <div class="panel-body">
                        <form action="{{route('articles.update' , ['id' => $article->id])}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                {{method_field('PATCH')}}
                                @include('Admin.error')
                                <div class="form-group col-md-12">
                                    <label for="title">عنوان مقاله</label>
                                    <input type="text" class="form-control" placeholder="عنوان مقاله" name="title" id="title" value="{{$article->title}}">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="description">توضیحات</label>
                                    <textarea class="form-control" rows="5" name="description" id="description">{{$article->description}}</textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="body">متن</label>
                                    <textarea rows="5" class="form-control" placeholder="متن" name="body" id="body">{{$article->body}}</textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-sm-12">
                                        <label for="images">تصاویر</label>
                                        <input type="file" class="form-control" placeholder="تصاویر" name="images" id="images">
                                    </div>
                                    <div class="col-sm-12">
                                <div class="row">
                                    @foreach($article->images['images'] as $key => $image)
                                        <div class="col-sm-2">
                                            <label class="control-label">
                                               {{ $key }}
                                               <input type="radio" name="imagesThumb" value="{{ $image }}" {{ $article->images['thumb'] == $image ? 'checked' : '' }} />
                                               <a href="{{$image}}" target="_blank"><img src="{{$image}}" width="100%"></a>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">

                            <div class="col-sm-6">
                                <label for="tags">برچسب ها</label>
                                <input type="text" class="form-control" placeholder="برچسب ها" name="tags" id="tags" value="{{$article->tags}}">
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="submit" value="ویرایش مقاله" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END BORDERED TABLE -->
                </div>
            </div>
        </div>
    </div>


@endsection

@section('footer')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        window.onload = function() {
            CKEDITOR.replace( 'body', {
                filebrowserUploadUrl: '{{ route('upload',['_token' => csrf_token() ]) }}'
            });
        };
    </script>
@endsection