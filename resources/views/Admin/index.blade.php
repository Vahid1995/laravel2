@extends('Admin.master')

@section('content')

    <div class="panel panel-headline">
                    <div class="panel-heading">
                        <h3 class="panel-title">پیشخوان</h3>
                        <p class="panel-subtitle">23 خرداد 1395</p>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="metric">
                                    <span class="icon"><i class="fa fa-download"></i></span>
                                    <p>
                                        <span class="number">1,252</span>
                                        <span class="title">دانلودها</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="metric">
                                    <span class="icon"><i class="fa fa-shopping-bag"></i></span>
                                    <p>
                                        <span class="number">203</span>
                                        <span class="title">فروش کالا</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="metric">
                                    <span class="icon"><i class="fa fa-eye"></i></span>
                                    <p>
                                        <span class="number">274,678</span>
                                        <span class="title">بازدید</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="metric">
                                    <span class="icon"><i class="fa fa-bar-chart"></i></span>
                                    <p>
                                        <span class="number">35%</span>
                                        <span class="title">تعامل</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div id="headline-chart" class="ct-chart"></div>
                            </div>
                            <div class="col-md-3">
                                <div class="weekly-summary text-right">
                                    <span class="number">2,315</span> <span class="percentage"><i class="fa fa-caret-up text-success"></i> 12%</span>
                                    <span class="info-label">فروش کلی</span>
                                </div>
                                <div class="weekly-summary text-right">
                                    <span class="number">5,758<span style="font-size: 14px">تومان</span></span> <span class="percentage"><i class="fa fa-caret-up text-success"></i> 23%</span>
                                    <span class="info-label">درآمد ماهانه</span>
                                </div>
                                <div class="weekly-summary text-right">
                                    <span class="number">65,938<span style="font-size: 14px">تومان</span></span> <span class="percentage"><i class="fa fa-caret-down text-danger"></i> 8%</span>
                                    <span class="info-label">درآمد کلی</span>
                                </div>
                            </div>
                        </div>
                    </div>
    </div>


@endsection