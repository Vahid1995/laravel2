@extends('Admin.master')

@section('content')

    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title">ایجاد دوره</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- BORDERED TABLE -->
                    <div class="panel">

                        <div class="panel-body">
                            <form action="{{ route('courses.store') }}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                @include('Admin.error')
                                <div class="form-group col-md-12">
                                    <label for="title">عنوان دوره</label>
                                    <input type="text" class="form-control" placeholder="عنوان دوره" name="title" id="title" value="{{old('title')}}">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="title">وضعیت دوره</label>
                                    <select id="type" name="type" class="form-control">
                                        <option value="vip">اعضای ویژه</option>
                                        <option value="free" selected>رایگان</option>
                                        <option value="cach">نقدی</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="body">متن</label>
                                    <textarea rows="5" class="form-control" placeholder="متن" name="body" id="body">{{old('body')}}</textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="body">قیمت دوره</label>
                                    <input class="form-control" placeholder="قیمت دوره" name="price" id="price" value="{{old('price')}}">
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-sm-6">
                                        <label for="tags">برچسب ها</label>
                                        <input type="text" class="form-control" placeholder="برچسب ها" name="tags" id="tags" value="{{old('tags')}}">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="images">تصاویر</label>
                                        <input type="file" class="form-control" placeholder="تصاویر" name="images" id="images">
                                    </div>

                                </div>
                                <div class="form-group col-md-12">
                                   <input type="submit" value="ثبت دوره" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END BORDERED TABLE -->
                </div>
            </div>
        </div>
    </div>


@endsection

@section('footer')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        window.onload = function() {
            CKEDITOR.replace( 'body', {
                filebrowserUploadUrl: '{{ route('upload',['_token' => csrf_token() ]) }}'
            });
        };
    </script>
@endsection

