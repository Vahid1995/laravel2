@extends('Admin.master')

@section('content')

    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title">دوره ها</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- BORDERED TABLE -->
                    <div class="panel">
                        <div class="panel-heading">
                            <a href="{{route('courses.create')}}"  class="btn btn-primary"> دوره جدید</a>
                            <a href="{{route('episodes.index')}}"  class="btn btn-danger">بخش ویدئوها</a>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <thead>
                                <tr>

                                    <th>عنوان دوره</th>
                                    <th>تعداد نظرات</th>
                                    <th>مقدار بازدید</th>
                                    <th>تعداد افراد شرکت کننده</th>
                                    <th>وضعیت دوره</th>
                                    <th>تنظیمات</th>

                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($courses as $course)
                                        <tr>
                                            <td><a href="{{$course->path()}}">{{$course->title}}</a> </td>
                                            <td>{{$course->commentCount}}</td>
                                            <td>{{$course->viewCount}}</td>
                                            <td>1</td>
                                            <td>
                                                @if($course->type=='free')
                                                    رایگان
                                                @elseif($course->type=='vip')
                                                    اعضای ویژه
                                                @else
                                                نقدی
                                                @endif
                                            </td>
                                            <td>
                                                <form action="{{ route('courses.destroy' , ['id' => $course->id]) }}" method="post">
                                                    {{method_field('delete')}}
                                                    {{csrf_field()}}
                                                    <div class="btn-group btn-group-lg">
                                                        <a href="{{route('courses.edit',['id' => $course->id])}}"  class="btn-sm btn-primary">ویرایش</a>
                                                        <button type="submit" class="btn-sm btn-danger">حذف</button>
                                                    </div>

                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div style="text-align: center">
                            {{ $courses->render() }}
                        </div>
                    </div>
                    <!-- END BORDERED TABLE -->
                </div>
            </div>
        </div>
    </div>


@endsection