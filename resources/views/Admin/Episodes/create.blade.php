@extends('Admin.master')

@section('content')

    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title">ایجاد ویدئو</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- BORDERED TABLE -->
                    <div class="panel">

                        <div class="panel-body">
                            <form action="{{ route('episodes.store') }}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                @include('Admin.error')
                                <div class="form-group col-md-12">
                                    <label for="title">عنوان ویدئو</label>
                                    <input type="text" class="form-control" placeholder="عنوان ویدئو" name="title" id="title" value="{{old('title')}}">
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-sm-6">
                                        <label for="course_id">دوره ی مربوطه</label>
                                        <select class="form-control" name="course_id" id="course_id">
                                            @foreach(\App\Course::latest()->get() as $course)

                                                <option value="{{ $course->id }}">{{ $course->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="title">وضعیت ویدئو</label>
                                        <select id="type" name="type" class="form-control">
                                            <option value="vip">اعضای ویژه</option>
                                            <option value="free" selected>رایگان</option>
                                            <option value="cach">نقدی</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="description">متن</label>
                                    <textarea rows="5" class="form-control" placeholder="متن" name="description" id="description">{{old('description')}}</textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-sm-6">
                                        <label for="videoUrl">لینک ویدئو</label>
                                        <input class="form-control" placeholder="لینک ویدئو" name="videoUrl" id="videoUrl" value="{{old('videoUrl')}}">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="number">شماره ویدئو</label>
                                        <input class="form-control" placeholder="شماره ویدئو" name="number" id="number" value="{{old('number')}}">
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-sm-6">
                                        <label for="tags">برچسب ها</label>
                                        <input type="text" class="form-control" placeholder="برچسب ها" name="tags" id="tags" value="{{old('tags')}}">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="time">زمان ویدئو</label>
                                        <input type="text" class="form-control" placeholder="زمان" name="time" id="time" value="{{old('time')}}">
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                   <input type="submit" value="ارسال ویدئو" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END BORDERED TABLE -->
                </div>
            </div>
        </div>
    </div>


@endsection

@section('footer')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        window.onload = function() {
            CKEDITOR.replace( 'description', {
                filebrowserUploadUrl: '{{ route('upload',['_token' => csrf_token() ]) }}'
            });
        };
        $(document).ready(function(){
            $('#course_id').selectpicker({
                noneSelectedText : 'لطفا دوره ی مورد نظر را انتخاب کنید'
            });
        });
    </script>
@endsection

