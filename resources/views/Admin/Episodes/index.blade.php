@extends('Admin.master')

@section('content')

    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title">ویدئوها</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- BORDERED TABLE -->
                    <div class="panel">
                        <div class="panel-heading">
                            <a href="{{route('episodes.create')}}"  class="btn btn-primary"> ویدئو جدید</a>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <thead>
                                <tr>

                                    <th>عنوان ویدئو</th>
                                    <th>تعداد نظرات</th>
                                    <th>مقدار بازدید</th>
                                    <th>تعداد دانلود</th>
                                    <th>وضعیت ویدئو</th>
                                    <th>تنظیمات</th>

                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($episodes as $episode)
                                        <tr>
                                            <td><a href="{{$episode->path()}}">{{$episode->title}}</a> </td>
                                            <td>{{$episode->commentCount}}</td>
                                            <td>{{$episode->viewCount}}</td>
                                            <td>{{ $episode->downloadCount }}</td>
                                            <td>
                                                @if($episode->type=='free')
                                                    رایگان
                                                @elseif($episode->type=='vip')
                                                    اعضای ویژه
                                                @else
                                                نقدی
                                                @endif
                                            </td>
                                            <td>
                                                <form action="{{ route('episodes.destroy' , ['id' => $episode->id]) }}" method="post">
                                                    {{method_field('delete')}}
                                                    {{csrf_field()}}
                                                    <div class="btn-group btn-group-lg">
                                                        <a href="{{route('episodes.edit',['id' => $episode->id])}}"  class="btn-sm btn-primary">ویرایش</a>
                                                        <button type="submit" class="btn-sm btn-danger">حذف</button>
                                                    </div>

                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div style="text-align: center">
                            {{ $episodes->render() }}
                        </div>
                    </div>
                    <!-- END BORDERED TABLE -->
                </div>
            </div>
        </div>
    </div>


@endsection
